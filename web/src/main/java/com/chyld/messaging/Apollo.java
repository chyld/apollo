package com.chyld.messaging;

import com.chyld.dtos.UserDto;
import com.chyld.entities.Position;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;

@Service
public class Apollo {
    @Autowired
    private RabbitTemplate template;

    @Autowired
    private TopicExchange topic;

    public void send(){
        HashMap<String, Object> hm = new HashMap<>();
        hm.put("a", 3);
        hm.put("b", 5);
        hm.put("c", 7);

        Position p = new Position();
        p.setLatitude(3);
        p.setLongitude(5);
        p.setAltitude(7);

        UserDto u = new UserDto("bob", Arrays.asList("a", "b", "c"));

        String name = topic.getName();
        this.template.convertAndSend(name, "fit.topic.run.start", u);
        this.template.convertAndSend(name, "fit.topic.run.stop", u);
        this.template.convertAndSend(name, "fit.topic.run.water", u);
        // this.template.convertAndSend(name, "fit.topic.pos", hm);
    }
}
