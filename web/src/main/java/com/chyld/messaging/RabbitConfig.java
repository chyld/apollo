package com.chyld.messaging;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


@Configuration
public class RabbitConfig {

    @Bean
    public TopicExchange topic() {
        return new TopicExchange("fit.exchange");
    }

    @Bean
    public Queue runQueue() {
        return new Queue("fit.queue.run");
    }

    @Bean
    public Queue posQueue() {
        return new Queue("fit.queue.pos");
    }

    @Bean
    public Binding binding1a(TopicExchange topic, Queue runQueue) {
        return BindingBuilder.bind(runQueue).to(topic).with("fit.topic.run.*");
    }

    @Bean
    public Binding binding1b(TopicExchange topic, Queue posQueue) {
        return BindingBuilder.bind(posQueue).to(topic).with("fit.topic.pos");
    }
}
