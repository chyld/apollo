package com.chyld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComputeMain {
    public static void main(String[] args) {
        SpringApplication.run(ComputeMain.class, args);
    }
}
